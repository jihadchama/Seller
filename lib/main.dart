import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'src/app.dart';

import 'package:seller/src/core/external/logger/logger.dart';
import 'package:seller/src/core/external/configs/start_config.dart';
import 'package:seller/src/core/presentation/bloc/app/app_bloc.dart';

Future<void> main() async => await runZonedGuarded<Future<void>>(
      () async {
        await startApplicationDependencies();

        Bloc.observer = AppBlocObserver(Log.instance);
        runApp(const App());
      },
      (Object exception, StackTrace stack) => recordError(exception, stack),
    );
