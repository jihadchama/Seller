import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';

import 'package:connectivity_plus/connectivity_plus.dart';

import 'package:seller/src/core/presentation/routes/routers.dart';
import 'package:seller/src/core/presentation/themes/dark_theme.dart';
import 'package:seller/src/core/presentation/themes/light_theme.dart';
import 'package:seller/src/core/presentation/widgets/snack_widget.dart';

import 'package:seller/src/utils/localization/localization_config.dart';

import 'package:seller/src/core/presentation/extensions/context_extension.dart';

import 'package:seller/src/modules/connectivity/domain/usecases/connectivity_usecase.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: '',
      restorationScopeId: 'app',
      debugShowCheckedModeBanner: false,

      /// Languages
      supportedLocales: supportedLocales,
      localizationsDelegates: localizationsDelegates,
      onGenerateTitle: (BuildContext context) => context.localize.appTitle,

      /// Themes
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: ThemeMode.system,

      /// Routes
      routerDelegate: rootRoutes.routerDelegate,
      routeInformationParser: rootRoutes.routeInformationParser,
      routeInformationProvider: rootRoutes.routeInformationProvider,

      /// Connections
      builder: (context, child) {
        final network = GetIt.I.get<ConnectivityUseCase>();

        network.onConnectivityChanged().listen((state) {
          if (state == ConnectivityResult.none) {
            snackBarWidget(
              context: context,
              message: 'Not Conected',
            );
          }
        });

        return child!;
      },
    );
  }
}
