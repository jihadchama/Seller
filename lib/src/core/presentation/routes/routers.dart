import 'package:go_router/go_router.dart';

import 'package:seller/src/modules/auth/presentation/routes/routers.dart';

import 'package:seller/src/modules/splash/presentation/routes/routers.dart';
import 'package:seller/src/modules/splash/presentation/screens/splash_screen.dart';

final GoRouter rootRoutes = GoRouter(
  debugLogDiagnostics: true,
  urlPathStrategy: UrlPathStrategy.path,
  initialLocation: SplashScreen.values.path,
  routes: <GoRoute>[
    ...splashRoutes,
    ...authRoutes,
  ],
);
