part of 'connection_bloc.dart';

@immutable
abstract class ConnectionState extends Equatable {
  @override
  List<Object?> get props => [];
}

@immutable
class ConnectionInitialState extends ConnectionState {}

@immutable
class NoConnectionState extends ConnectionState {
  late final String message;

  NoConnectionState(this.message);

  @override
  List<Object?> get props => [message];
}

@immutable
class ConnectionFailureState extends ConnectionState {
  late final String message;

  ConnectionFailureState(this.message);

  @override
  List<Object?> get props => [message];
}
