import 'package:bloc/bloc.dart';

import 'package:flutter/foundation.dart';

import 'package:equatable/equatable.dart';

import 'package:bloc_concurrency/bloc_concurrency.dart';

import 'package:connectivity_plus/connectivity_plus.dart';

import 'package:seller/src/modules/connectivity/domain/usecases/connectivity_usecase.dart';

part 'connection_event.dart';
part 'connection_state.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionState> {
  late final ConnectivityUseCase _connectivityUseCase;

  ConnectionBloc(
    this._connectivityUseCase,
  ) : super(ConnectionInitialState()) {
    on<VerifyConnectionEvent>(
      _verifyConnectivity,
      transformer: droppable(),
    );
  }

  void _verifyConnectivity(
    VerifyConnectionEvent event,
    Emitter<ConnectionState> emit,
  ) {
    _connectivityUseCase.onConnectivityChanged().listen((status) {
      if (status != ConnectivityResult.mobile ||
          status != ConnectivityResult.wifi) {
        emit(NoConnectionState('No connection'));
      }
    }).onError((_) => emit(ConnectionFailureState('Error verify connection')));
  }
}
