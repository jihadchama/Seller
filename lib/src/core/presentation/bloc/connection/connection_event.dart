part of 'connection_bloc.dart';

@immutable
abstract class ConnectionEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

@immutable
class VerifyConnectionEvent extends ConnectionEvent {}
