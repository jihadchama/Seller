// ignore_for_file: unnecessary_overrides

import 'package:bloc/bloc.dart';
import 'package:seller/src/core/external/logger/logger.dart';

class AppBlocObserver extends BlocObserver {
  final Log _log;

  AppBlocObserver(this._log);

  @override
  void onChange(BlocBase bloc, Change change) {
    _log.debug('$change');
    super.onChange(bloc, change);
  }

  @override
  void onClose(BlocBase bloc) {
    super.onClose(bloc);
  }

  @override
  void onCreate(BlocBase bloc) {
    _log.debug('$bloc');
    super.onCreate(bloc);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
  }
}
