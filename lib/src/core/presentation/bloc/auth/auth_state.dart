part of 'auth_bloc.dart';

@immutable
abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

@immutable
class AuthInitialState extends AuthState {}

@immutable
class AuthLoadingState extends AuthState {}

@immutable
class AuthThereIsAccessTokenState extends AuthState {}

@immutable
class AuthThereIsNoAccessTokenState extends AuthState {}

@immutable
class AuthStatus extends AuthState {
  final MeEntity? _me;
  final MeStatus _meStatus;

  AuthStatus._(this._me, this._meStatus);

  AuthStatus.unauthenticated() : this._(null, MeStatus.authenticated);

  AuthStatus.authenticated(MeEntity me) : this._(me, MeStatus.authenticated);

  @override
  List<Object?> get props => [_me, _meStatus];
}
