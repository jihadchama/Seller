part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

@immutable
class AuthVerifyAccessTokenEvent extends AuthEvent {}

@immutable
class AuthChanged extends AuthEvent {
  final MeEntity _me;

  AuthChanged(this._me);

  @override
  List<Object?> get props => [_me];
}
