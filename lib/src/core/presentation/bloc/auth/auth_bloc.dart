import 'package:bloc/bloc.dart';

import 'package:flutter/foundation.dart';

import 'package:equatable/equatable.dart';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:seller/src/modules/http/domain/usecases/http_post_usecase.dart';

import 'package:seller/src/utils/constants/app_constant.dart';

import 'package:seller/src/modules/auth/domain/entities/me/me_entity.dart';
import 'package:seller/src/modules/storage/domain/usecases/storage_usecase.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> with ChangeNotifier {
  late final StorageUseCase _storageUseCase;
  late final HttpPostUseCase _httpPostUseCase;

  AuthBloc(
    this._storageUseCase,
    this._httpPostUseCase,
  ) : super(AuthInitialState()) {
    on<AuthVerifyAccessTokenEvent>(
      _verifyAccessToken,
      transformer: droppable(),
    );
    on<AuthChanged>(
      _verifyAuth,
      transformer: droppable(),
    );
  }

  void _verifyAccessToken(
    AuthVerifyAccessTokenEvent event,
    Emitter<AuthState> emit,
  ) {
    String accessToken = _getAccessToken();

    accessToken.isNotEmpty
        ? emit(AuthThereIsAccessTokenState())
        : emit(AuthThereIsNoAccessTokenState());
  }

  Future<void> _verifyAuth(
    AuthChanged event,
    Emitter<AuthState> emit,
  ) async {
    String accessToken = _getAccessToken();

    //final value = await _httpPostUseCase(const HttpParams(url: 'url'));
  }

  String _getAccessToken() {
    final data = _storageUseCase.get(key: AppContants.keyAccessToken);

    return data.isRight ? data.right : '';
  }
}
