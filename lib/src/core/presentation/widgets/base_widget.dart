import 'package:flutter/material.dart';

import 'package:seller/src/core/presentation/extensions/keyboard_mixin.dart';

class BaseScaffold extends StatelessWidget with KeyboardMixin {
  final Widget child;

  const BaseScaffold({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => hideKeyboard(context),
          child: child,
        ),
      ),
    );
  }
}
