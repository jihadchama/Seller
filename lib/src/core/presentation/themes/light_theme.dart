import 'package:flutter/material.dart';

import 'package:seller/src/core/presentation/themes/color_schemes.dart';

final lightTheme = ThemeData(
  useMaterial3: true,
  colorScheme: lightColorScheme,
  primarySwatch: Colors.blueGrey,
  applyElevationOverlayColor: true,
  cardColor: const Color(0xff1b1b1b),
  canvasColor: const Color(0xff1b1b1b),
  backgroundColor: const Color(0xff1b1b1b),
  bottomAppBarColor: const Color(0xff1b1b1b),
  unselectedWidgetColor: const Color(0xb3ffffff),
  visualDensity: VisualDensity.adaptivePlatformDensity,
  buttonTheme: const ButtonThemeData(
    height: 36,
    minWidth: 88,
    alignedDropdown: false,
    colorScheme: lightColorScheme,
    layoutBehavior: ButtonBarLayoutBehavior.padded,
    padding: EdgeInsets.only(
      top: 0,
      left: 16,
      right: 16,
      bottom: 0,
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.elliptical(2.0, 2.0),
        topRight: Radius.elliptical(2.0, 2.0),
        bottomLeft: Radius.elliptical(2.0, 2.0),
        bottomRight: Radius.elliptical(2.0, 2.0),
      ),
    ),
  ),
);
