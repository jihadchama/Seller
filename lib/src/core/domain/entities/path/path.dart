class PathScreen {
  late final String path;
  late final String name;

  PathScreen({
    required this.path,
    required this.name,
  });
}
