import 'package:get_it/get_it.dart';

import 'package:seller/src/core/external/di/module.dart';

import 'package:seller/src/core/presentation/bloc/auth/auth_bloc.dart';
import 'package:seller/src/core/presentation/bloc/connection/connection_bloc.dart';
import 'package:seller/src/modules/http/domain/usecases/http_post_usecase.dart';

import 'package:seller/src/modules/storage/domain/usecases/storage_usecase.dart';
import 'package:seller/src/modules/connectivity/domain/usecases/connectivity_usecase.dart';

class GlobalModule implements Module {
  final GetIt locator;

  GlobalModule(this.locator);

  @override
  Future<void> initialized() async {
    /// Auth
    locator.registerLazySingleton<AuthBloc>(
      () => AuthBloc(
        locator.get<StorageUseCase>(),
        locator.get<HttpPostUseCase>(),
      ),
    );

    /// Connection
    locator.registerLazySingleton<ConnectionBloc>(
      () => ConnectionBloc(
        locator.get<ConnectivityUseCase>(),
      ),
    );
  }
}
