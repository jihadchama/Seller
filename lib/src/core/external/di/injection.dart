import 'package:get_it/get_it.dart';

import 'package:seller/src/core/external/di/global.dart';
import 'package:seller/src/core/external/di/module.dart';

import 'package:seller/src/modules/auth/external/di/injection.dart';
import 'package:seller/src/modules/http/external/di/injection.dart';
import 'package:seller/src/modules/storage/external/di/injection.dart';
import 'package:seller/src/modules/firabase/external/di/injection.dart';
import 'package:seller/src/modules/connectivity/external/di/injection.dart';

class ServiceLocator implements Module {
  final GetIt locator;

  ServiceLocator(this.locator);

  @override
  Future<void> initialized() async {
    await FirebaseModule(locator).initialized();
    await StorageModule(locator).initialized();
    await HttpModule(locator).initialized();
    await ConnectivityModule(locator).initialized();
    await GlobalModule(locator).initialized();
    await AuthModule(locator).initialized();
  }
}
