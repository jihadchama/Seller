import 'package:get_it/get_it.dart';

import 'package:flutter/widgets.dart';

import 'package:seller/src/core/external/di/injection.dart';

import 'package:seller/src/modules/firabase/external/configs/firebase_config.dart';

Future<void> startApplicationDependencies() async {
  WidgetsFlutterBinding.ensureInitialized();
  final serviceLocator = GetIt.instance;

  await FirebaseConfig.configurationInitialized();
  await ServiceLocator(serviceLocator).initialized();

  // await _configurationNetworkInitialized(serviceLocator);
}

dynamic recordError(Object exception, StackTrace stack) =>
    FirebaseConfig.recordError(exception, stack);

/*Future<void> _configurationNetworkInitialized(GetIt locator) async {
  final network = locator.get<ConnectivityUseCase>();

  network.onConnectivityChanged().listen((state) {
    print('''

    State: $state 

  ''');
  });
}*/

// Future<void> _configurationDynamicLinkInitialized(GetIt locator) async {
//   final dynamicLink = locator.get<DynamicLinkUseCase>();

//   final initial = await _initialLink(dynamicLink);
//   // dynamicLink.backgroundLink().listen((event) { });

//   print('''

//     INITIAL: $initial

//     BACKGROUND: 

//   ''');
// }

// Future<RouteSettings> _initialLink(DynamicLinkUseCase dynamicLink) async {
//   final value = await dynamicLink.initialLink();

//   return value.isRight
//       ? RouteSettings(name: 'link', arguments: value.right)
//       : const RouteSettings(name: '', arguments: Object());
// }
