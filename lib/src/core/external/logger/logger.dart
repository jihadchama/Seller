// ignore_for_file: depend_on_referenced_packages

import 'package:logger/logger.dart';

class Log {
  Log._();

  static Log? _instance;
  static Log get instance => _instance ??= Log._();

  final Logger _logger = Logger(
    printer: PrettyPrinter(
      colors: true,
      methodCount: 2,
      lineLength: 120,
      printTime: false,
      printEmojis: true,
      errorMethodCount: 8,
    ),
  );

  void debug(String message) {
    _logger.d(message);
  }
}
