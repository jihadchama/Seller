import 'package:connectivity_plus/connectivity_plus.dart';

abstract class ConnectivityClient {
  Future<bool> call();
  Stream<ConnectivityResult> onConnectivityChanged();
}
