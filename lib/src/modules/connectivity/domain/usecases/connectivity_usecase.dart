import 'package:connectivity_plus/connectivity_plus.dart';

abstract class ConnectivityUseCase {
  Future<bool> call();
  Stream<ConnectivityResult> onConnectivityChanged();
}
