import 'package:either_dart/either.dart';

import 'package:seller/src/modules/http/domain/params/http_params.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

abstract class HttpPostUseCase {
  Future<Either<HttpException, Map<String, dynamic>>> call(HttpParams params);
}
