import 'package:either_dart/either.dart';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

import 'package:seller/src/modules/firabase/data/clients/dynamic_link_client.dart';

class DynamicLinkAdapter implements DynamicLinkClient {
  late final FirebaseDynamicLinks _dynamicLinks;

  DynamicLinkAdapter(this._dynamicLinks);

  @override
  Stream<PendingDynamicLinkData> backgroundLink() => _dynamicLinks.onLink;

  @override
  Future<Either<DynamicLinkException, String>> initialLink() async {
    final PendingDynamicLinkData? value = await _dynamicLinks.getInitialLink();

    return value != null
        ? Right(value.link.path)
        : Left(DynamicLinkException(message: ''));
  }
}
