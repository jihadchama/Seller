import 'package:either_dart/either.dart';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

import 'package:seller/src/modules/firabase/data/clients/dynamic_link_client.dart';
import 'package:seller/src/modules/firabase/domain/usecases/dynamic_link_usecase.dart';

class RemoteDynamicLinkUseCase implements DynamicLinkUseCase {
  late final DynamicLinkClient _dynamicLinkClient;

  RemoteDynamicLinkUseCase(this._dynamicLinkClient);

  @override
  Stream<PendingDynamicLinkData> backgroundLink() =>
      _dynamicLinkClient.backgroundLink();

  @override
  Future<Either<DynamicLinkException, String>> initialLink() async =>
      await _dynamicLinkClient.initialLink();
}
