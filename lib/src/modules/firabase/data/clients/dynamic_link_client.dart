import 'package:either_dart/either.dart';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

abstract class DynamicLinkClient {
  Stream<PendingDynamicLinkData> backgroundLink();
  Future<Either<DynamicLinkException, String>> initialLink();
}
