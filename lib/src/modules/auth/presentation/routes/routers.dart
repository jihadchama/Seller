import 'package:go_router/go_router.dart';

import 'package:seller/src/modules/auth/presentation/screens/auth/me/me_screen.dart';
import 'package:seller/src/modules/auth/presentation/screens/auth/signup/signup_screen.dart';

final authRoutes = <GoRoute>[
  GoRoute(
    name: SignUpScreen.values.name,
    path: SignUpScreen.values.path,
    builder: (context, state) => SignUpScreen(),
  ),
  GoRoute(
    name: MeScreen.values.name,
    path: MeScreen.values.path,
    builder: (context, state) => const MeScreen(),
  ),
];
