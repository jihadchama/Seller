import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:seller/src/utils/constants/app_constant.dart';

import 'package:seller/src/core/domain/entities/path/path.dart';
import 'package:seller/src/core/presentation/widgets/base_widget.dart';
import 'package:seller/src/core/presentation/extensions/keyboard_mixin.dart';
import 'package:seller/src/core/presentation/extensions/widget_extension.dart';

import 'package:seller/src/modules/http/domain/params/http_params.dart';
import 'package:seller/src/modules/auth/presentation/screens/auth/me/me_screen.dart';
import 'package:seller/src/modules/auth/presentation/screens/auth/signup/bloc/signup_bloc.dart';

class SignUpScreen extends StatelessWidget with KeyboardMixin {
  static PathScreen values = PathScreen(name: 'signup', path: '/signup');

  final _emailFocus = FocusNode();
  final _passwordFocus = FocusNode();
  final _formKey = GlobalKey<FormState>();

  final params = const HttpParams(
    body: {
      'email': 'gjramos100@gmail.com',
      'password': 'cicada3301',
    },
    url: AppContants.urlAuthSignUp,
  );

  SignUpScreen({
    Key? key,
  }) : super(key: key);

  void _navigateTo(String name, BuildContext context) {
    context.replaceNamed(name);
  }

  void _fieldFocusChange(
    BuildContext context,
    FocusNode currentFocus,
    FocusNode nextFocus,
  ) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = locator.get<SignUpBloc>();

    return BaseScaffold(
      child: BlocListener<SignUpBloc, SignUpState>(
        bloc: bloc,
        listener: (_, SignUpState state) {
          if (state is SignUpSuccessState) {
            _navigateTo(MeScreen.values.name, context);
          }
        },
        child: BlocBuilder<SignUpBloc, SignUpState>(
          bloc: bloc,
          builder: (_, SignUpState state) {
            if (state is SignUpLoadingState) {
              return const Center(
                child: CircularProgressIndicator.adaptive(),
              );
            }

            if (state is SignUpFailureState) {
              return Center(
                child: Text(state.message),
              );
            }

            return Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text('Sign UP'),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      focusNode: _emailFocus,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      onFieldSubmitted: (_) {
                        _fieldFocusChange(
                          context,
                          _emailFocus,
                          _passwordFocus,
                        );
                      },
                      decoration: const InputDecoration(
                        labelText: 'E-mail',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    TextFormField(
                      obscureText: true,
                      focusNode: _passwordFocus,
                      keyboardType: TextInputType.url,
                      textInputAction: TextInputAction.done,
                      onFieldSubmitted: (_) {
                        _passwordFocus.unfocus();

                        bloc.add(SignUpAwnerEvent(params));
                      },
                      decoration: const InputDecoration(
                        labelText: 'Password',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    ElevatedButton(
                      onPressed: () {
                        bloc.add(SignUpAwnerEvent(params));
                      },
                      child: const Text('Enter'),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
