import 'package:lottie/lottie.dart';

import 'package:flutter/material.dart';
import 'package:seller/src/core/domain/entities/path/path.dart';

class SplashScreen extends StatelessWidget {
  static PathScreen values = PathScreen(name: 'splash', path: '/');

  // final _debounce = CallDebounce(milliseconds: 3000);

  const SplashScreen({Key? key}) : super(key: key);

  /*void _navigateTo(BuildContext context, String path) {
    _debounce.run(
      () {
        Navigator.pushNamedAndRemoveUntil(
          context,
          path,
          (Route<dynamic> route) => false,
        );
      },
    );
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Lottie.asset(
            'assets/animations/splash.json',
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}
