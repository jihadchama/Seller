import 'package:go_router/go_router.dart';

import 'package:seller/src/modules/splash/presentation/screens/splash_screen.dart';

final splashRoutes = <GoRoute>[
  GoRoute(
    name: SplashScreen.values.name,
    path: SplashScreen.values.path,
    builder: (context, state) => const SplashScreen(),
  ),
];
