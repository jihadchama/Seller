import 'package:mocktail/mocktail.dart';
import 'package:either_dart/either.dart';

import 'package:seller/src/modules/http/data/clients/http_client.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

class SignUpUseCaseSpy extends Mock implements HttpClient {
  When mockSigUp() => when(() => post(params: any(named: 'params')));

  void mockSuccess(Either<HttpException, Map<String, dynamic>> value) =>
      mockSigUp().thenAnswer((_) async => value);

  void mockFailure(Either<HttpException, Map<String, dynamic>> value) =>
      mockSigUp().thenAnswer((_) async => value);
}
