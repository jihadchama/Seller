import 'package:either_dart/either.dart';

import 'package:seller/src/core/domain/entities/exception/exception.dart';

class HttpMockResponse {
  static Either<HttpException, Map<String, dynamic>> buildJsonSuccess() =>
      const Right({'response': 'ok'});
}
